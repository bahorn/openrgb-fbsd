--- Controllers/CorsairHydroController/CorsairHydroControllerDetect.cpp.orig	2020-12-07 03:18:48 UTC
+++ Controllers/CorsairHydroController/CorsairHydroControllerDetect.cpp
@@ -3,7 +3,7 @@
 #include "RGBController.h"
 #include "RGBController_CorsairHydro.h"
 #include <vector>
-#include <libusb-1.0/libusb.h>
+#include <libusb.h>
 
 /*-----------------------------------------------------*\
 | Corsair vendor ID                                     |
