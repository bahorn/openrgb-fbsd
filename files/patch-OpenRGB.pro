--- OpenRGB.pro.orig	2020-12-07 03:18:48 UTC
+++ OpenRGB.pro
@@ -638,7 +638,7 @@ win32:contains(QMAKE_TARGET.arch, x86) {
 #-----------------------------------------------------------------------------------------------#
 # Linux-specific Configuration                                                                  #
 #-----------------------------------------------------------------------------------------------#
-unix:!macx {
+linux {
     TARGET = $$lower($$TARGET)
 
     INCLUDEPATH +=                                                                              \
@@ -699,7 +699,7 @@ unix:!macx {
     INSTALLS += target desktop pixmap rules
 }
 
-unix:!macx:CONFIG(asan) {
+linux:CONFIG(asan) {
     message("ASan Mode")
     QMAKE_CFLAGS=-fsanitize=address
     QMAKE_CXXFLAGS=-fsanitize=address
@@ -725,6 +725,26 @@ unix:macx {
 
     CONFIG +=                                                                                   \
     c++14                                                                                       \
+}
+
+
+#-----------------------------------------------------------------------------------------------#
+# FreeBSD-specific Configuration                                                                #
+#-----------------------------------------------------------------------------------------------#
+
+freebsd {
+    SOURCES +=                                                                                  \
+    serial_port/find_usb_serial_port_linux.cpp                                                  \
+
+
+    INCLUDEPATH +=                                                                              \
+    /usr/local/include                                                                          \
+
+    LIBS +=                                                                                     \
+    -L/usr/local/lib -lusb -lhidapi                                                             \
+
+    CONFIG +=                                                                                   \
+    c++14   
 }
 
 DISTFILES += \
