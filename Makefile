# $FreeBSD$

PORTNAME=	openrgb
DISTVERSION=	0.5
CATEGORIES=	deskutils

MAINTAINER=	b@horn.uk
COMMENT=	Open source RGB lighting control

LICENSE=	GPLv2

LIB_DEPENDS= 	libhidapi.so:comms/hidapi
BUILD_DEPENDS=	pkgconf:devel/pkgconf

USES=		qmake:outsource qt:5 gl
USE_QT=		buildtools_build gui widgets core
USE_GL+=	gl

USE_GITLAB=	yes
GL_ACCOUNT=	CalcProgrammer1
GL_PROJECT=	OpenRGB
GL_COMMIT= 	88464d159571a433d50a9de67b48c6b1623920a7

PATCHDIR=files/

do-install:
	${INSTALL_PROGRAM} ${CONFIGURE_WRKSRC}/OpenRGB ${STAGEDIR}${PREFIX}/bin

.include <bsd.port.mk>
